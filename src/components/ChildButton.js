import styled from "styled-components";
import SuperButton from "./SuperButton";

const ChildButton = styled(SuperButton)`
    background-color: ${props => props.bg};
`;

export default ChildButton;