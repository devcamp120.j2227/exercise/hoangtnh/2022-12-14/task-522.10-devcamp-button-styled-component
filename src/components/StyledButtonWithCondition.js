import styled from "styled-components";

const StyledButtonWithCondition = styled.button`
    ${props => props.type && props.type === "primary" ? 
        `background-color: purple; color: white;`: 
        `background-color: pink; color: black;`}
    padding: 10px;
    border: none;
    border-radius: 5px;
    font-size: 1.25rem;
`;

export default StyledButtonWithCondition;